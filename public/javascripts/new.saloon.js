﻿$( function () {
    
    $( '#newsaloon' ).click( function () {
        
        $(
            '<div class="modal fade" tabindex="-1" role="dialog">' +
            '   <div class="modal-dialog" role="document">' +
            '       <div class="modal-content">' +
            
            '           <div class="modal-header">' +
            '               <h5 class="modal-title">Créer un salon</h5>' +
            '               <button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
            '                   <span aria-hidden="true">&times;</span>' +
            '               </button>' +
            '           </div>' +
            
            '           <div class="modal-body">' +
            
            '               <div class="form-group">' +
            '                   <label>Nom du salon</label>' +
            '                   <input type="text" class="form-control" maxlength="32" >' +
            '               </div>' +
            
            '               <div class="form-group">' +
            '                   <label>Mot de passe du salon</label>' +
            '                   <input type="password" class="form-control" >' +
            '               </div>' +
            
            '           </div>' +
            
            '           <div class="modal-footer">' +
            '               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>' +
            '               <button type="button" class="btn btn-primary">Save changes</button>' +
            '           </div>' +
            
            '       </div>' +
            '   </div>' +
            '</div>'
        ).modal( 'show' );
        
    } );
    
} );